# Fonts
adjust_column_width     0
adjust_line_height      0
bold_font               auto
bold_italic_font        auto
box_drawing_scale       0.001, 1, 1.5, 2
disable_ligatures       always
font_family             Noto Mono
font_features           none
font_size               15.0
force_ltr               no
italic_font             auto
# Cursor customization.
cursor                  #cccccc
cursor_text_color       #111111
cursor_shape            beam
cursor_beam_thickness   1.5
cursor_underline_thickness 2.0
cursor_blink_interval   0
cursor_stop_blinking_after 15.0
# Scrollback
scrollback_lines        10000
scrollback_pager        less --chop-long-lines --RAW-CONTROL-CHARS +INPUT_LINE_NUMBER
scrollback_pager_history_size 0
wheel_scroll_multiplier 5.0
touch_scroll_multiplier 1.0
# Mouse
mouse_hide_wait         0.0
url_color               #0087bd
url_style               single
open_url_modifiers      kitty_mod
open_url_with           default
url_prefixes            http https file ftp
detect_urls             yes
copy_on_select          clipboard
strip_trailing_spaces   never
rectangle_select_modifiers kitty_mod+shift
terminal_select_modifiers kitty_mod
select_by_word_characters @-./_~?&=%+#
click_interval          -1.0
focus_follows_mouse     no
pointer_shape_when_grabbed arrow
default_pointer_shape   beam
pointer_shape_when_dragging beam
# Performance
repaint_delay           10
input_delay             3
sync_to_monitor         yes
# Terminal bell
enable_audio_bell       yes
visual_bell_duration    0.0
window_alert_on_bell    no
bell_on_tab             yes
command_on_bell         none
# Window layout
remember_window_size    no
initial_window_width    1500
initial_window_height   1500
enabled_layouts         vertical,horizontal
## Possible values: fat, grid, horizontal, splits, stack, tall, vertical
window_resize_step_cells 2
window_resize_step_lines 2
window_border_width     3px
draw_minimal_borders    no
window_margin_width     0
single_window_margin_width -1
window_padding_width    0
placement_strategy      center
active_border_color     #505075
inactive_border_color   #252525
bell_border_color       #A0A0A0
inactive_text_alpha     0.50
hide_window_decorations no
resize_debounce_time    0.1
resize_draw_strategy    static
resize_in_steps         no
confirm_os_window_close 0
# Tab bar
tab_bar_edge            bottom
tab_bar_margin_width    0.0
tab_bar_style           powerline
tab_bar_min_tabs        2
tab_switch_strategy     previous
tab_fade                0.25 0.5 0.75 1
tab_separator           " | "
tab_activity_symbol     "•"
tab_title_template      "{index}:{title}"
active_tab_title_template none
active_tab_foreground   #000
active_tab_background   #eee
active_tab_font_style   bold-italic
inactive_tab_foreground #444
inactive_tab_background #999
inactive_tab_font_style normal
tab_bar_background      none
# Color scheme
## Using git repository "kitty-themes" (https://github.com/dexpota/kitty-themes)
## to manage color schemes. Include file contents.
include                 kitty-themes/themes/onedark.conf
background_opacity      1.0
background_image        none
## Must be in PNG format.
background_image_layout tiled
background_image_linear no
dynamic_background_opacity no
background_tint         0.0
dim_opacity             0.90
# Advanced configuration
shell                   .
editor                  .
close_on_child_death    no
allow_remote_control    yes
listen_on               none
## env MYVAR1=a
## env MYVAR2=${MYVAR1}/${HOME}/.config
update_check_interval   24
startup_session         none
clipboard_control       write-clipboard write-primary
allow_hyperlinks        yes
term                    xterm-kitty
# OS specific tweaks
macos_titlebar_color    background
macos_option_as_alt     both
macos_hide_from_tasks   no
macos_quit_when_last_window_closed no
macos_window_resizable  yes
macos_thicken_font      0
macos_traditional_fullscreen no
macos_show_window_title_in all
macos_custom_beam_cursor no
linux_display_server    auto
# Keyboard shortcuts
clear_all_shortcuts yes
kitty_mod cmd
map kitty_mod+c         copy_to_clipboard
map kitty_mod+v         paste_from_clipboard
map kitty_mod+shift+c   pass_selection_to_program
map kitty_mod+up        scroll_line_up
map kitty_mod+down      scroll_line_down
map kitty_mod+page_up   scroll_page_up
map kitty_mod+page_down scroll_page_down
map kitty_mod+home      scroll_home
map kitty_mod+end       scroll_end
map kitty_mod+i         show_scrollback
map kitty_mod+shift+v   launch --cwd=current vim
map kitty_mod+enter     new_window_with_cwd
map kitty_mod+n         new_os_window_with_cwd
map kitty_mod+shift+n   new_os_window
map kitty_mod+w         close_window
map kitty_mod+shift+w   close_os_window
map kitty_mod+o         next_window
map kitty_mod+shift+o   previous_window
map kitty_mod+f         move_window_forward
map kitty_mod+b         move_window_backward
map kitty_mod+alt+t     move_window_to_top
map kitty_mode+1        first_window
map kitty_mode+2        second_window
map kitty_mode+3        third_window
map kitty_mode+4        fourth_window
map kitty_mode+5        fifth_window
map kitty_mode+6        sixth_window
map kitty_mode+7        seventh_window
map kitty_mode+8        eighth_window
map kitty_mode+9        ninth_window
map kitty_mode+0        tenth_window
map kitty_mod+t         new_tab
map kitty_mod+right     next_tab
map kitty_mod+left      previous_tab
map kitty_mod+d         close_tab
map kitty_mod+/         move_tab_forward
map kitty_mod+.         move_tab_backward
map kitty_mod+shift+t   set_tab_title
map kitty_mode+shift+1  goto_tab 1
map kitty_mode+shift+2  goto_tab 2
map kitty_mode+shift+3  goto_tab 3
map kitty_mode+shift+4  goto_tab 4
map kitty_mode+shift+5  goto_tab 5
map kitty_mode+shift+6  goto_tab 6
map kitty_mode+shift+7  goto_tab 7
map kitty_mode+shift+8  goto_tab 8
map kitty_mode+shift+9  goto_tab 9
map kitty_mode+shift+0  goto_tab 10
map kitty_mod+l         next_layout
map kitty_mod+shift+l   previous_layout
map kitty_mod+s         goto_layout stack
map kitty_mod+equal     change_font_size current +1.0
map kitty_mod+plus      change_font_size current +1.0
map kitty_mod+minus     change_font_size current -1.0
map kitty_mod+0         change_font_size current 0
map kitty_mod+shift+0   change_font_size all 0
map kitty_mod+e         kitten hints
map kitty_mod+p>u       kitten hints --type hyperlink
map kitty_mod+p>f       kitten hints --type file
map kitty_mod+p>c       kitten hints --type file --program -
map kitty_mod+p>l       kitten hints --type line --program -
map kitty_mod+p>h       kitten hints --type hash --program -
map alt+f10             toggle_maximized
map kitty_mod+f10       toggle_maximized
map kitty_mod+m         toggle_maximized
map kitty_mod+shift+m   toggle_maximized
map kitty_mod+f11       toggle_fullscreen
map kitty_mod+u         kitten unicode_input
map kitty_mod+escape    kitty_shell window
