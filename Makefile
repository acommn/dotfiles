# Default target, this should be kept at the top of this Makefile.
all : emacs \
      mpv

# Recover changes made to these files if that's the case.
emacs : ${HOME}/.emacs \
	      ${HOME}/.local/bin/el
	mkdir -p emacs ;							# Make sure directory structure is there.
	cp ${HOME}/.emacs ./emacs/emacs ;
	cp ${HOME}/.local/bin/el ./emacs/el ;

# Recover changes made to these files if that's the case.
mpv : ${HOME}/.config/mpv/input.conf \
      ${HOME}/.config/mpv/mpv.conf \
      ${HOME}/.config/mpv/script-opts/osc.conf
	mkdir -p mpv/script-opts ;		# Make sure directory structure is there.
	cp ${HOME}/.config/mpv/input.conf ./mpv/input.conf ;
	cp ${HOME}/.config/mpv/mpv.conf ./mpv/mpv.conf ;
	cp ${HOME}/.config/mpv/script-opts/osc.conf ./mpv/script-opts/osc.conf ;

# Installation of all configuration files in this repository, this target
# (install) should be kept at the bottom of this Makefile.
install :
#######################
# Emacs section start #
#######################
# Install Emacs initialization file.
	install --compare --no-target-directory ./emacs/emacs ${HOME}/.emacs ;
# Install Emacs Launcher (el).
	install --directory ${HOME}/.local/bin ;
	install --compare ./emacs/el ${HOME}/.local/bin/ ;
#####################
# MPV section start #
#####################
# Create directory structure.
	install --directory ${HOME}/.config/mpv/script-opts ;
# Install mpv's input handling configuration file.
	install --compare ./mpv/input.conf ${HOME}/.config/mpv/ ;
# Install mpv's configuration file.
	install --compare ./mpv/mpv.conf ${HOME}/.config/mpv/ ;
# Install mpv's on-screen-controller configuration file.
	install --compare ./mpv/script-opts/osc.conf ${HOME}/.config/mpv/script-opts/ ;
